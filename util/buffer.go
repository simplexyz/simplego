package util

import (
	"bytes"
	"sync"
)

var gBufferPool sync.Pool

func GetBuffer() *bytes.Buffer {
	buffer, ok := gBufferPool.Get().(*bytes.Buffer)
	if ok {
		return buffer
	}
	return &bytes.Buffer{}
}

func PutBuffer(b *bytes.Buffer) {
	if b == nil {
		return
	}
	b.Reset()
	gBufferPool.Put(b)
}
