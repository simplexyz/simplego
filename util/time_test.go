package util

import (
	"testing"
	"time"
)

func Test(t *testing.T) {
	now := time.Now()
	//t.Log(now.AddDate(0, 0, -8))
	t.Log(GetTheDayBeginTime(now, -37))
	date := time.Date(now.Year(), 2, 11, 0, 0, 0, 0, now.Location())
	t.Log(date)
	t.Log(GetTheWeekBeginTime(date))
}

func Test_IsSameDayUnixTimeStamp(t *testing.T) {
	samples := map[int64]int64{
		1567132490: 1567132491,
		1567102490: 1547102490,
		1546271999: 1546272000,
		1546272000: 1546272001,
	}

	for t1, t2 := range samples {
		t.Log(time.Unix(t1, 0))
		t.Log(time.Unix(t2, 0))
		t.Log(IsSameDayUnixTimeStamp(t1, t2))
	}

}
