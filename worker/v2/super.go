package worker

import (
	sadefine "gitee.com/simplexyz/simplego/actor/define"
	satimer "gitee.com/simplexyz/simplego/actor/timer"
	sdefine "gitee.com/simplexyz/simplego/define"
)

type Super struct {
	sdefine.IWorker
}

func (s *Super) Create(impl sdefine.IWorkerImplement, optionFuncs ...OptionFunc) {
	s.IWorker = Create(impl, optionFuncs...)
}

// Destroy 子类要么不覆盖，要么确保调用到父类的Destroy，否则会循环引用导致内存泄露
func (s *Super) Destroy() {
	s.IWorker = nil
}

func (s *Super) OnStarted(ctx sadefine.Context) {

}

func (s *Super) OnStopping(ctx sadefine.Context) {

}

func (s *Super) OnStopped(ctx sadefine.Context) {

}

func (s *Super) OnReceiveMessage(ctx sadefine.Context) {

}

func (s *Super) OnActorTerminated(who *sadefine.PID, ctx sadefine.Context) {

}

func (s *Super) OnRestarting(ctx sadefine.Context) {

}

func (s *Super) OnRestarted(ctx sadefine.Context) {

}

func (s *Super) BeforeTriggerTimer(id satimer.ID, tag satimer.Tag, ctx sadefine.Context) (ok bool, err error) {
	ok = true
	return
}

func (s *Super) AfterTriggerTimer(err error, id satimer.ID, tag satimer.Tag, ctx sadefine.Context) {

}
