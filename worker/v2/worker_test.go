package worker

import (
	"errors"
	sawork "gitee.com/simplexyz/simplego/actor/work"
	"log"
	"sync"
	"testing"
	"time"

	sactor "gitee.com/simplexyz/simplego/actor"
	sadefine "gitee.com/simplexyz/simplego/actor/define"
	satimer "gitee.com/simplexyz/simplego/actor/timer"
	srandom "gitee.com/simplexyz/simplego/random"
)

type example struct {
	Super

	startedWg sync.WaitGroup
	stoppedWg sync.WaitGroup
}

func createExample() *example {
	e := &example{}

	e.Super.Create(e,
		WithStartedWaitGroup(&e.startedWg),
		WithStoppedWaitGroup(&e.stoppedWg),
		WithMailBoxSize(10),
	)

	return e
}

func (e *example) OnReceiveMessage(ctx sadefine.Context) {
	switch msg := ctx.Message().(type) {
	case string:
		log.Printf("recv string: %s", msg)
		switch msg {
		case "restart test":
			log.Panic(msg)
		}
	default:
		log.Printf("recv %#v", msg)
	}
}

func (e *example) OnStarted(ctx sadefine.Context) {
	log.Println("OnStarted")
}

func (e *example) OnStopping(ctx sadefine.Context) {
	log.Println("OnStopping")
}

func (e *example) OnStopped(ctx sadefine.Context) {
	log.Println("OnStopped")
}

func (e *example) OnRestarting(ctx sadefine.Context) {
	log.Println("OnRestarting")
}

func (e *example) OnRestarted(ctx sadefine.Context) {
	log.Println("OnRestarted")
}

func (e *example) BeforeTriggerTimer(id satimer.ID, tag satimer.Tag, ctx sadefine.Context) (ok bool, err error) {
	log.Printf("BeforeTriggerTimer, id=%d, tag=%d", id, tag)
	return true, nil
}

func (e *example) AfterTriggerTimer(err error, id satimer.ID, tag satimer.Tag, ctx sadefine.Context) {
	log.Printf("AfterTriggerTimer, id=%d, tag=%d", id, tag)
}

func Test(t *testing.T) {
	sactor.Init("root", sactor.ResumeDecider)
	defer sactor.Final()

	e := createExample()

	err := e.Start(nil, "example", func() error {
		log.Print("startFunc")
		return nil
	}, true)
	if err != nil {
		t.Errorf("start fail, %s", err)
		return
	}

	e.NewLoopTimer(1*time.Second, 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
		sactor.RootContext().Send(e.PID(), srandom.String(16))
	})

	e.NewTimer(srandom.Duration(1*time.Second, 3*time.Second), 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
		sactor.RootContext().Send(e.PID(), "restart test")
	})

	if err := e.Post(sawork.PostFunc(func(ctx sadefine.Context) {
		log.Print("Post")
	})); err != nil {
		t.Errorf("Post fail, %v", e)
		return
	}

	if err := e.Dispatch(0, sawork.DispatchFunc(func(ctx sadefine.Context) error {
		time.Sleep(1 * time.Second)
		log.Print("Dispatch")
		return errors.New("dispatch error test")
	})); err != nil {
		if err.Error() != "dispatch error test" {
			t.Errorf("Dispatch fail, %v", err)
			return
		}
	} else {
		t.Errorf("Dispatch fail, err is nil")
		return
	}

	time.Sleep(5 * time.Second)

	err = e.Stop(func() {
		log.Print("stopFunc")
	}, true)
	if err != nil {
		t.Errorf("Stop fail, %s", err)
		return
	}
}
