package worker

import (
	"sync"

	sactor "gitee.com/simplexyz/simplego/actor"
	sadefine "gitee.com/simplexyz/simplego/actor/define"
	sdefine "gitee.com/simplexyz/simplego/define"
)

type option struct {
	impl sdefine.IWorkerImplement

	startedWg *sync.WaitGroup
	stoppedWg *sync.WaitGroup

	mailBoxSize int

	decider sadefine.DeciderFunc
}

type OptionFunc func(option *option)

func newOption(impl sdefine.IWorkerImplement) option {
	return option{
		impl:    impl,
		decider: sactor.ResumeDecider,
	}
}

func WithStartedWaitGroup(startedWg *sync.WaitGroup) OptionFunc {
	return func(option *option) {
		option.startedWg = startedWg
	}
}

func WithStoppedWaitGroup(stoppedWg *sync.WaitGroup) OptionFunc {
	return func(option *option) {
		option.stoppedWg = stoppedWg
	}
}

func WithMailBoxSize(mailBoxSize int) OptionFunc {
	return func(option *option) {
		if mailBoxSize > 0 {
			option.mailBoxSize = mailBoxSize
		}
	}
}

func WithDecider(decider sadefine.DeciderFunc) OptionFunc {
	return func(option *option) {
		if decider != nil {
			option.decider = decider
		}
	}
}
