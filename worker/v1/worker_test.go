package worker

import (
	"context"
	"log"
	"sync"
	"testing"
	"time"

	sdebug "gitee.com/simplexyz/simplego/debug"
)

func Test(t *testing.T) {
	w, err := Create(nil, true,
		WithOnStarted(func() {
			log.Println("onStarted")
		}),
		WithOnWorking(func(ctx context.Context) bool {
			var ticker = time.NewTicker(1 * time.Second)
			var count int
			for {
				select {
				case <-ctx.Done():
					ticker.Stop()
					return false

				case <-ticker.C:
					count++

					log.Printf("onWorking, count=%d\n", count)

					ticker.Reset(1 * time.Second)
				}
			}
		}),
		WithOnStop(func() {
			log.Println("onStopping")
		}),
		WithOnError(func(p any) (err error) {
			sdebug.WriteStackTraceFile(p, "")
			return nil
		}),
	)
	if err != nil {
		t.Errorf("start fail, %v", err)
		return
	}

	log.Println("started")

	if err := w.start(nil, false); err == nil {
		t.Errorf("must return already started")
		return
	}

	time.Sleep(3 * time.Second)

	w.Stop(nil, true)

	log.Println("stopped")

	if w.Working() {
		t.Error("must not working")
		return
	}
}

type example struct {
	*Worker
	ctx         context.Context
	cancelFunc  context.CancelFunc
	startWaiter sync.WaitGroup
	stopWaiter  sync.WaitGroup
	noError     bool
	ticker      *time.Ticker
	count       int
}

func newExample(noError bool) *example {
	e := &example{
		startWaiter: sync.WaitGroup{},
		stopWaiter:  sync.WaitGroup{},
		noError:     noError,
	}

	e.ctx, e.cancelFunc = context.WithCancel(context.Background())

	var err error
	e.Worker, err = Create(nil, true,
		WithContext(e.ctx, e.cancelFunc),
		WithStartWaiter(&e.startWaiter),
		WithStopWaiter(&e.stopWaiter),
		WithOnStarted(e.onStarted),
		WithOnWorking(e.onWorking),
		WithOnStop(e.onStopping),
		WithOnError(e.onError),
	)
	if err != nil {
		log.Panic(err)
	}

	return e
}

func (e *example) onStarted() {
	log.Println("onStarted")
	e.ticker = time.NewTicker(1 * time.Second)
}

func (e *example) onWorking(ctx context.Context) bool {
	for {
		select {
		case <-ctx.Done():
			return false

		case <-e.ticker.C:
			e.ticker.Reset(1 * time.Second)

			e.count++
			log.Printf("onWorking, count=%d\n", e.count)
			if e.count%3 == 0 {
				if !e.noError {
					panic("test")
				}
			}
		}
	}
}

func (e *example) onStopping() {
	log.Println("onStopping")
	e.ticker.Stop()
}

func (e *example) onError(p any) error {
	log.Printf("onError, %v", p)
	sdebug.WriteStackTraceFile(p, "../worker")
	return nil
}

func (e *example) Stop(wait bool) {
	e.Worker.Stop(e.cancelFunc, wait)
}

func Test_Example(t *testing.T) {
	e := newExample(true)

	if err := e.start(nil, true); err != nil {
		t.Errorf("start fail, %v", err)
		return
	}
	if err := e.start(nil, false); err == nil {
		t.Errorf("must return already started")
		return
	}

	time.Sleep(5 * time.Second)

	e.Stop(true)

	if e.Working() {
		t.Error("must not working")
		return
	}
}

func Test_Example_OnError(t *testing.T) {
	e := newExample(false)

	if err := e.start(nil, true); err != nil {
		t.Errorf("start fail, %v", err)
		return
	}

	time.Sleep(15 * time.Second)

	e.Stop(true)

	if e.Working() {
		t.Error("must not working")
		return
	}
}
