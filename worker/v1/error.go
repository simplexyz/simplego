package worker

import "errors"

var (
	ErrAlreadyStarted = errors.New("already started")
	ErrNoOnWorking    = errors.New("no onWorking")
)
