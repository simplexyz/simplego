//go:build ignore

package define

import (
	"time"

	aactor "github.com/asynkron/protoactor-go/actor"

	sahttpreq "gitee.com/simplexyz/simplego/actor/http/request"
	satimer "gitee.com/simplexyz/simplego/actor/timer"
	slog "gitee.com/simplexyz/simplego/log/v1"
)

// IActor 供Super子类调用的接口
type IActor interface {
	PID() *aactor.PID
	ParentPID() *aactor.PID

	Sign() string
	// SetSign 线程不安全
	SetSign(sign string)

	Start(ctx aactor.Context, name string, startFunc func() error, wait bool) (err error)
	HasStop() bool
	Stop(stopFunc func(), wait bool) error

	Logger() slog.ILogger
	Debug(args ...any)
	Debugf(format string, args ...any)
	Info(args ...any)
	Infof(format string, args ...any)
	Warn(args ...any)
	Warnf(format string, args ...any)
	Error(args ...any)
	Errorf(format string, args ...any)

	NewTimer(dur time.Duration, tag satimer.Tag, cb satimer.Callback) satimer.ID
	NewLoopTimer(dur time.Duration, tag satimer.Tag, cb satimer.Callback) satimer.ID
	StopTimer(id satimer.ID) error

	NewHttpRequest(timeout time.Duration, tag sahttpreq.Tag, onPrepareFunc sahttpreq.OnPrepareFunc, onCompletedFunc sahttpreq.OnCompletedFunc) (requestID sahttpreq.ID, err error)
}

// IActorImplement 供Actor调用的接口
type IActorImplement interface {
	Destroy()

	OnStarted(ctx aactor.Context)
	OnStopping(ctx aactor.Context)
	OnStopped(ctx aactor.Context)
	OnReceiveMessage(ctx aactor.Context)
	OnActorTerminated(who *aactor.PID, ctx aactor.Context)
	OnRestarting(aactor.Context)
	OnRestarted(ctx aactor.Context)

	BeforeTriggerTimer(id satimer.ID, tag satimer.Tag, ctx aactor.Context) (ok bool, err error)
	AfterTriggerTimer(err error, id satimer.ID, tag satimer.Tag, ctx aactor.Context)

	BeforeCompleteHttpRequest(id sahttpreq.ID, tag sahttpreq.Tag, ctx aactor.Context) (ok bool, err error)
	AfterCompleteHttpRequest(err error, id sahttpreq.ID, tag sahttpreq.Tag, ctx aactor.Context)
}
