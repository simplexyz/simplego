//go:build ignore

package define

import (
	sldefine "gitee.com/simplexyz/simplego/log"
	"time"

	sadefine "gitee.com/simplexyz/simplego/actor/define"
	sahttpreq "gitee.com/simplexyz/simplego/actor/http/request"
)

// IEntity 供Super子类调用的接口
type IEntity interface {
	IWorker

	Logger() sldefine.ILogger
	Debug(args ...any)
	Debugf(format string, args ...any)
	Info(args ...any)
	Infof(format string, args ...any)
	Warn(args ...any)
	Warnf(format string, args ...any)
	Error(args ...any)
	Errorf(format string, args ...any)

	NewHttpRequest(timeout time.Duration, tag sahttpreq.Tag, onPrepareFunc sahttpreq.OnPrepareFunc, onCompletedFunc sahttpreq.OnCompletedFunc) (requestID sahttpreq.ID, err error)
}

// IEntityImplement 供Entity调用的接口
type IEntityImplement interface {
	IWorkerImplement

	BeforeCompleteHttpRequest(id sahttpreq.ID, tag sahttpreq.Tag, ctx sadefine.Context) (ok bool, err error)
	AfterCompleteHttpRequest(err error, id sahttpreq.ID, tag sahttpreq.Tag, ctx sadefine.Context)
}
