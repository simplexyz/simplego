package define

import (
	sadefine "gitee.com/simplexyz/simplego/actor/define"
	satimer "gitee.com/simplexyz/simplego/actor/timer"
)

type (
	OnActorStartedFunc          = func(ctx sadefine.Context)
	OnActorStoppingFunc         = func(ctx sadefine.Context)
	OnActorStoppedFunc          = func(ctx sadefine.Context)
	OnActorTerminatedFunc       = func(ctx sadefine.Context, who *sadefine.PID, why sadefine.TerminatedReason)
	OnActorRestartingFunc       = func(ctx sadefine.Context)
	BeforeActorTriggerTimerFunc = func(id satimer.ID, tag satimer.Tag, ctx sadefine.Context) (ok bool, err error)
	AfterActorTriggerTimerFunc  = func(err error, id satimer.ID, tag satimer.Tag, ctx sadefine.Context)
	OnActorReceiveMessageFunc   = func(ctx sadefine.Context) (continued bool)
)
