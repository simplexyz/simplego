package define

import (
	"time"

	sadefine "gitee.com/simplexyz/simplego/actor/define"
	satimer "gitee.com/simplexyz/simplego/actor/timer"
	sawork "gitee.com/simplexyz/simplego/actor/work"
)

// IWorker 供Super子类调用的接口
type IWorker interface {
	PID() *sadefine.PID

	Start(ctx sadefine.Context, name string, startFunc func() error, wait bool) (err error)
	HasStart() bool
	Started() bool
	Stop(stopFunc func(), wait bool) error
	HasStop() bool

	Post(work sawork.IPostWork) error
	Dispatch(timeout time.Duration, work sawork.IDispatchWork) error

	NewTimer(dur time.Duration, tag satimer.Tag, cb satimer.Callback) satimer.ID
	NewLoopTimer(dur time.Duration, tag satimer.Tag, cb satimer.Callback) satimer.ID
	StopTimer(id satimer.ID) error
}

// IWorkerImplement 供Worker调用的接口
type IWorkerImplement interface {
	Destroy()

	OnStarted(ctx sadefine.Context)
	OnStopping(ctx sadefine.Context)
	OnStopped(ctx sadefine.Context)
	OnReceiveMessage(ctx sadefine.Context)
	OnActorTerminated(who *sadefine.PID, ctx sadefine.Context)
	OnRestarting(ctx sadefine.Context)
	OnRestarted(ctx sadefine.Context)

	BeforeTriggerTimer(id satimer.ID, tag satimer.Tag, ctx sadefine.Context) (ok bool, err error)
	AfterTriggerTimer(err error, id satimer.ID, tag satimer.Tag, ctx sadefine.Context)
}
