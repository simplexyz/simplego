package debug

import "testing"

func Test_WriteStackTraceFile(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			WriteStackTraceFile(r, ".")
		}
	}()

	panic("test")
}
