package debug

import (
	"fmt"
	"net/http"
	"testing"
	"time"
)

func TestStartPprofServer(t *testing.T) {
	// 随机绑定端口
	addr, err := StartPprofServer("", 0)
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Printf("start pprof server in %s\n", addr)

	time.Sleep(5 * time.Second)

	RegisterCommand("/test", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("test"))
	})

	time.Sleep(60 * time.Second)

	StopPprofServer()

	fmt.Println("stop pprof server")
}
