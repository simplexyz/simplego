package main

import (
	"C"

	slog "gitee.com/simplexyz/simplego/log"
)

//export Init
func Init(dir *C.char, name *C.char) {
	slog.Init(false, &slog.LoggerOption{
		Name:   name,
		Level:  slog.LevelDebug,
		Caller: false,
	})
}

//export Final
func Final() {
	slog.Final()
}

//export Debug
func Debug(text *C.char) {
	slog.Debug(C.GoString(text))
}

//export Info
func Info(text *C.char) {
	slog.Info(C.GoString(text))
}

//export Warn
func Warn(text *C.char) {
	slog.Warn(C.GoString(text))
}

//export Error
func Error(text *C.char) {
	slog.Error(C.GoString(text))
}

func main() {

}
