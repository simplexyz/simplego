@set WORK_DIR=%~dp0

@IF "%1" == "" call :x64 & cd %WORK_DIR% & goto :exit

@IF "%1" == "x64" call :x64 & cd %WORK_DIR% & goto :exit

@IF "%1" == "x86" call :x86 & cd %WORK_DIR% & goto :exit

@echo unsupported operate [%1]

@goto :exit

:x64
@echo [x64] begin
go env -w GOARCH=amd64
go env -w CGO_ENABLED=1
go build -buildmode=c-shared -o slog64.dll
go env -w CGO_ENABLED=0
@echo [x64] end
@goto :exit

:x86
@echo [x86] begin
go env -w GOARCH=386
go env -w CGO_ENABLED=1
go build -buildmode=c-shared -o slog32.dll
go env -w GOARCH=amd64
go env -w CGO_ENABLED=0
@echo [x86] end
@goto :exit

:exit
