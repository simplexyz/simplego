package log

import (
	"fmt"
	"log"
	"os"
	"sync/atomic"
)

type consoleLogger struct {
	level  atomic.Int32
	logger *log.Logger
}

func CreateConsoleLogger() ILogger {
	l := &consoleLogger{
		logger: log.New(os.Stdout, "", log.LstdFlags),
	}
	l.level.Store(int32(LevelDebug))
	return l
}

func (l *consoleLogger) Destroy() {

}

func (l *consoleLogger) SetLevel(level Level) {
	l.level.Store(int32(level))
}

func (l *consoleLogger) Debug(args ...interface{}) {
	if LevelDebug > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[D] %s\n", fmt.Sprint(args...))
}

func (l *consoleLogger) Debugf(format string, args ...interface{}) {
	if LevelDebug > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[D] %s\n", fmt.Sprintf(format, args...))
}

func (l *consoleLogger) Info(args ...interface{}) {
	if LevelInfo > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[I] %s\n", fmt.Sprint(args...))
}

func (l *consoleLogger) Infof(format string, args ...interface{}) {
	if LevelInfo > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[I] %s\n", fmt.Sprintf(format, args...))
}

func (l *consoleLogger) Warn(args ...interface{}) {
	if LevelWarn > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[W] %s\n", fmt.Sprint(args...))
}

func (l *consoleLogger) Warnf(format string, args ...interface{}) {
	if LevelWarn > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[W] %s\n", fmt.Sprintf(format, args...))
}

func (l *consoleLogger) Error(args ...interface{}) {
	if LevelError > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[E] %s\n", fmt.Sprint(args...))
}

func (l *consoleLogger) Errorf(format string, args ...interface{}) {
	if LevelError > Level(l.level.Load()) {
		return
	}
	l.logger.Printf("[E] %s\n", fmt.Sprintf(format, args...))
}
