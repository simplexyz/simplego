package log

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	sadefine "gitee.com/simplexyz/simplego/actor/define"
	sutil "gitee.com/simplexyz/simplego/util"
	aactor "github.com/asynkron/protoactor-go/actor"
)

type ILogger interface {
	Destroy()
	SetLevel(level Level)
	Debug(args ...any)
	Debugf(format string, args ...any)
	Info(args ...any)
	Infof(format string, args ...any)
	Warn(args ...any)
	Warnf(format string, args ...any)
	Error(args ...any)
	Errorf(format string, args ...any)
}

type LoggerOption struct {
	Name            string
	Level           Level
	TimestampFormat string
	Caller          bool
	CallerSkip      int
}

type logger struct {
	LoggerOption

	level atomic.Int32

	writers []IWriter

	startedOnce sync.Once
	startedWg   sync.WaitGroup

	stoppedOnce sync.Once

	pid *sadefine.PID

	count sync.WaitGroup
}

func CreateLogger(option LoggerOption, writers ...IWriter) (ILogger, error) {
	l := &logger{
		LoggerOption: option,
	}

	var err error

	if l.Name == "" {
		l.Name, err = sutil.GetProgramFileBaseName()
		if err != nil {
			return nil, fmt.Errorf("get logger name fail, %w", err)
		}
	}
	if _, ok := loggers.Load(l.Name); ok {
		return nil, fmt.Errorf("logger[%s] already exist", l.Name)
	}

	if !l.Level.Valid() {
		l.Level = LevelDebug
	}
	l.level.Store(int32(l.Level))

	if l.TimestampFormat == "" {
		l.TimestampFormat = "2006/01/02T15:04:05.000"
	}

	for _, w := range writers {
		if w != nil {
			l.writers = append(l.writers, w)
		}
	}
	if len(l.writers) == 0 {
		l.writers = append(l.writers, ConsoleWriter())
	}

	l.startedWg.Add(1)

	props := aactor.PropsFromProducer(func() aactor.Actor { return l } /*, aactor.WithMailbox(aactor.Unbounded())*/)
	_, err = rootContext().SpawnNamed(props, l.Name)
	if err != nil {
		return nil, err
	}

	loggers.Store(l.Name, l)

	l.startedWg.Wait()

	return l, nil
}

func MustCreateLogger(option LoggerOption, writers ...IWriter) ILogger {
	l, err := CreateLogger(option, writers...)
	if err != nil {
		panic(err)
	}
	return l
}

func (l *logger) Destroy() {
	rootContext().Poison(l.pid)
	l.count.Wait()
}

func (l *logger) SetLevel(level Level) {
	if !level.Valid() {
		return
	}
	l.level.Store(int32(level))
}

func (l *logger) canOutput(level Level) bool {
	if !level.Valid() {
		return false
	}
	if level < Level(l.level.Load()) {
		return false
	}
	return true
}

func (l *logger) log(level Level, content string) {
	r := createRecord()

	r.timestamp = time.Now().Format(l.TimestampFormat)
	r.level = level
	r.content = content

	if l.Caller {
		var ok bool
		_, r.path, r.line, ok = runtime.Caller(l.CallerSkip)
		if !ok {
			r.path = "???"
			r.line = 0
		}
	}

	l.count.Add(1)

	rootContext().Send(l.pid, r)
}

func (l *logger) Receive(ctx sadefine.Context) {
	switch msg := ctx.Message().(type) {
	case *record:
		defer l.count.Done()

		msg.buildBytes()

		for i, w := range l.writers {
			if i == 0 {
				w.Write(msg)
			} else {
				w.Write(msg.Clone())
			}
		}

	case *sadefine.Started:
		l.startedOnce.Do(func() {
			l.pid = ctx.Self().Clone()

			l.startedWg.Done()
		})

	case *sadefine.Stopped:
		l.stoppedOnce.Do(func() {
			clear(l.writers)
		})
	}
}

func (l *logger) Debug(args ...any) {
	if !l.canOutput(LevelDebug) {
		return
	}
	m := fmt.Sprint(args...)
	l.log(LevelDebug, m)
}

func (l *logger) Debugf(format string, args ...any) {
	if !l.canOutput(LevelDebug) {
		return
	}
	m := fmt.Sprintf(format, args...)
	l.log(LevelDebug, m)
}

func (l *logger) Info(args ...any) {
	if !l.canOutput(LevelInfo) {
		return
	}
	m := fmt.Sprint(args...)
	l.log(LevelInfo, m)
}

func (l *logger) Infof(format string, args ...any) {
	if !l.canOutput(LevelInfo) {
		return
	}
	m := fmt.Sprintf(format, args...)
	l.log(LevelInfo, m)
}

func (l *logger) Warn(args ...any) {
	if !l.canOutput(LevelWarn) {
		return
	}
	m := fmt.Sprint(args...)
	l.log(LevelWarn, m)
}

func (l *logger) Warnf(format string, args ...any) {
	if !l.canOutput(LevelWarn) {
		return
	}
	m := fmt.Sprintf(format, args...)
	l.log(LevelWarn, m)
}

func (l *logger) Error(args ...any) {
	if !l.canOutput(LevelError) {
		return
	}
	m := fmt.Sprint(args...)
	l.log(LevelError, m)
}

func (l *logger) Errorf(format string, args ...any) {
	if !l.canOutput(LevelError) {
		return
	}
	m := fmt.Sprintf(format, args...)
	l.log(LevelError, m)
}
