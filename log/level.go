package log

const (
	_LevelBegin = iota            // begin
	LevelDebug  = 1 << (iota - 1) // 调试
	LevelInfo                     // 追踪
	LevelWarn                     // 警告
	LevelError                    // 错误
	_LevelEnd                     // end
	LevelAll    = LevelDebug | LevelInfo | LevelWarn | LevelError
)

var levels = []Level{LevelDebug, LevelInfo, LevelWarn, LevelError}

func EachLevel(fn func(level Level) (continued bool)) {
	for _, l := range levels {
		if !fn(l) {
			break
		}
	}
}

type Level int32 // Level 日志级别

func (l Level) Valid() bool {
	return l > _LevelBegin && l < _LevelEnd
}

// Prefix 每个级别的日志对应的prefix
func (l Level) Prefix() (prefix string) {
	switch l {
	case LevelError:
		prefix = "[E]"
	case LevelWarn:
		prefix = "[W]"
	case LevelInfo:
		prefix = "[I]"
	case LevelDebug:
		prefix = "[D]"
	}
	return
}

// SubName 需要区分级别存放日志信息时，用于获取每个级别日志存放的子目录
func (l Level) SubName() string {
	switch l {
	case LevelError:
		return "error"
	case LevelWarn:
		return "warn"
	case LevelInfo:
		return "info"
	case LevelDebug:
		return "debug"
	}
	return ""
}

func (l Level) IsIn(levels int32) bool {
	return int32(l)&levels == int32(l)
}
