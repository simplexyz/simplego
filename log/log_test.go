package log

import (
	"log"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func TestLog_Console(t *testing.T) {
	Init(true, nil)

	begin := time.Now()
	count := atomic.Int32{}

	defer func() {
		Final()

		var c = count.Load()

		log.Printf("count=[%d]", c)

		log.Printf("consume=[%d]ms\n", time.Since(begin).Milliseconds())
	}()

	l := MustGetLogger("default")

	wg := sync.WaitGroup{}

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			for i := 0; i < 10000; i++ {
				l.Debugf("%d hello %s", i+1, "world")
				l.Infof("%d hello %s", i+1, "world")
				l.Warnf("%d hello %s", i+1, "world")
				l.Errorf("%d hello %s", i+1, "world")
				count.Add(4)
			}
			wg.Done()
		}()
	}

	wg.Wait()
}

func TestLog_File(t *testing.T) {
	Init(false, &LoggerOption{Caller: true, CallerSkip: 2}, MustCreateFileWriter(&FileOption{
		Name:            "test",
		Dir:             "./log",
		SeperatedLevels: LevelError,
	}))

	begin := time.Now()
	count := atomic.Int32{}

	defer func() {
		Final()

		var c = count.Load()

		log.Printf("count=[%d]", c)

		log.Printf("consume=[%d]ms", time.Since(begin).Milliseconds())
	}()

	l := Default()

	wg := sync.WaitGroup{}
	num := 100000

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			for i := 0; i < num; i++ {
				tag := (i + 1) * 10
				l.Debugf("%d hello %s", tag+1, "world")
				l.Infof("%d hello %s", tag+2, "world")
				l.Warnf("%d hello %s", tag+3, "world")
				l.Errorf("%d hello %s", tag+4, "world")
				count.Add(4)
			}
			wg.Done()
		}()
	}

	wg.Wait()
}

func TestLog_Seq(t *testing.T) {
	Init(false, nil, MustCreateFileWriter(&FileOption{
		Name:            "test",
		Dir:             "./log",
		SeperatedLevels: LevelError,
	}))
	defer func() {
		Final()
	}()

	for i := 0; i < 10000; i++ {
		Debugf("%d hello %s", i+1, "world")
	}
}

func TestLog_RemoveTimeout(t *testing.T) {
	Init(false, nil, MustCreateFileWriter(&FileOption{
		Name:            "test",
		Dir:             "./log",
		MaxAge:          1 * time.Minute,
		SeperatedLevels: LevelError,
		CheckInterval:   2 * time.Second,
	}))

	defer func() {
		Final()
	}()

	time.Sleep(4 * time.Second)
}

func TestLog_MultiWriter(t *testing.T) {
	Init(true, &LoggerOption{Name: "logger", Caller: true, CallerSkip: 3}, ConsoleWriter(), MustCreateFileWriter(&FileOption{
		Name: "test",
		Dir:  "./log",
	}))
	defer func() {
		Final()
	}()

	for i := 0; i < 100; i++ {
		Debugf("%d hello %s", i+1, "world")
	}
}
