package log

type IWriter interface {
	Write(record IRecord)
	Destroy()
}
