package log

import (
	"sync"
	"sync/atomic"
	"testing"
)

func TestLogger_File(t *testing.T) {
	Init(false, nil)

	//begin := time.Now()
	count := atomic.Int32{}

	defer func() {
		Final()

		var c = count.Load()

		t.Logf("count=%d", c)

		//t.Logf("consume: %dms\n", time.Since(begin).Milliseconds())
	}()

	l, err := CreateLogger(LoggerOption{
		Name:       "test",
		Level:      LevelDebug,
		Caller:     true,
		CallerSkip: 2,
	},
	)
	if err != nil {
		t.Errorf("create defaultLogger fail, %v", err)
		return
	}

	wg := sync.WaitGroup{}
	num := 100000

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			for i := 0; i < num; i++ {
				tag := (i + 1) * 10
				l.Debugf("%d hello %s", tag+1, "world")
				l.Infof("%d hello %s", tag+2, "world")
				l.Warnf("%d hello %s", tag+3, "world")
				l.Errorf("%d hello %s", tag+4, "world")
				count.Add(4)
			}
			wg.Done()
		}()
	}

	wg.Wait()
}
