package log

import (
	sadefine "gitee.com/simplexyz/simplego/actor/define"
	aactor "github.com/asynkron/protoactor-go/actor"
)

var (
	//actorSystemOnce sync.Once
	actorSystem = aactor.NewActorSystem()
)

func rootContext() *sadefine.RootContext {
	//if actorSystem == nil {
	//	actorSystemOnce.Do(func() {
	//		actorSystem = aactor.NewActorSystem()
	//	})
	//}
	return actorSystem.Root
}
