package random

import "errors"

var (
	ErrWeightsIsEmpty     = errors.New("weights is empty")
	ErrTotalWeightsIsZero = errors.New("total weights is 0")
	ErrRawRatesIsEmpty    = errors.New("raw randomRates is empty")
	ErrCandidateIsEmpty   = errors.New("candidate is empty")
	ErrNotHit             = errors.New("not hit")
	ErrInvalidRate        = errors.New("invalid rate, must > 0 and <= 1")
)
