package actor

import (
	sadefine "gitee.com/simplexyz/simplego/actor/define"
)

func IsPIDEmpty(pid *sadefine.PID) bool {
	if pid == nil {
		return true
	}
	if pid.Address == "" || pid.Id == "" {
		return true
	}
	return false
}
