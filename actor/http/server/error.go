package server

import "errors"

var (
	ErrTooOften = errors.New("too often")
)
