package server

import (
	"github.com/gin-gonic/gin"
)

type (
	HandlerFunc = gin.HandlerFunc
	Context     = gin.Context
)
