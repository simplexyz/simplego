package request

import "errors"

var (
	ErrOnPrepareResultMustNotNil = errors.New("http request must not nil")
	ErrOnCompletedMustNotNil     = errors.New("http request on completed must not nil")
)
