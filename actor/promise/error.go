package promise

import "fmt"

type ErrNotExist struct {
	Seq Seq
}

func (e ErrNotExist) Error() string {
	return fmt.Sprintf("not exist, seq=%d", e.Seq)
}

type ErrTimeout struct {
	Seq              Seq
	RequestMessageID nrpc.MessageID
}

func (e ErrTimeout) Error() string {
	return fmt.Sprintf("timeout, seq=%d, request message id=%d", e.Seq, e.RequestMessageID)
}

type ErrProduceRequestMessageFail struct {
	Seq              Seq
	RequestMessageID nrpc.MessageID
	Err              error
}

func (e ErrProduceRequestMessageFail) Error() string {
	return fmt.Sprintf("produce request message fail, seq=%d, request message id=%d,  %s",
		e.Seq, e.RequestMessageID, e.Err)
}

type ErrUnmarshalRequestMessageFail struct {
	Seq              Seq
	RequestMessageID nrpc.MessageID
	Err              error
}

func (e ErrUnmarshalRequestMessageFail) Error() string {
	return fmt.Sprintf("unmarshal request message fail, seq=%d, request message id=%d, %s",
		e.Seq, e.RequestMessageID, e.Err)
}

type ErrProduceResponseMessageFail struct {
	Seq               Seq
	RequestMessageID  nrpc.MessageID
	ResponseMessageID nrpc.MessageID
	Err               error
}

func (e ErrProduceResponseMessageFail) Error() string {
	return fmt.Sprintf("produce resposne message fail, seq=%d, request message id=%d, response message id=%d, %s",
		e.Seq, e.RequestMessageID, e.ResponseMessageID, e.Err)
}

type ErrUnmarshalResponseMessageFail struct {
	Seq               Seq
	RequestMessageID  nrpc.MessageID
	ResponseMessageID nrpc.MessageID
	Err               error
}

func (e ErrUnmarshalResponseMessageFail) Error() string {
	return fmt.Sprintf("unmarshal resposne message fail, seq=%d, request message id=%d, response message id=%d, %s",
		e.Seq, e.RequestMessageID, e.ResponseMessageID, e.Err)
}

type ErrMarshalResponseMessageFail struct {
	Seq               Seq
	RequestMessageID  nrpc.MessageID
	ResponseMessageID nrpc.MessageID
	Err               error
}

func (e ErrMarshalResponseMessageFail) Error() string {
	return fmt.Sprintf("marshal resposne message fail, seq=%d, request message id=%d, response message id=%d, %s",
		e.Seq, e.RequestMessageID, e.ResponseMessageID, e.Err)
}
