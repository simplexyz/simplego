package actor

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"sync"
	"testing"
	"time"

	sadefine "gitee.com/simplexyz/simplego/actor/define"
	sahttpreq "gitee.com/simplexyz/simplego/actor/http/request"
	sahttpsvr "gitee.com/simplexyz/simplego/actor/http/server"
	satimer "gitee.com/simplexyz/simplego/actor/timer"
	slog "gitee.com/simplexyz/simplego/log"
	srandom "gitee.com/simplexyz/simplego/random"
)

func Test(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var startedWg sync.WaitGroup
	var stoppedWg sync.WaitGroup
	var logger slog.ILogger
	var timerMgr *satimer.Manager

	pid, err := Spawn(nil, true, "Test", false,
		WithStartedWaitGroup(&startedWg),
		WithStoppedWaitGroup(&stoppedWg),
		WithMailBoxSize(10),
		WithOnStartedFunc(func(ctx sadefine.Context) {
			logger = GetLogger(ctx)

			logger.Debug("OnStarted1")

			a := ctx.Actor().(*Actor)
			logger.Debugf("%#v", a)
		}, func(ctx sadefine.Context) {
			logger.Debug("OnStarted2")
			timerMgr = GetTimerManager(ctx)
		}),
		WithOnStoppingFunc(func(ctx sadefine.Context) {
			logger.Debug("OnStopping1")
		}, func(ctx sadefine.Context) {
			logger.Debug("OnStopping2")
		}),
		WithOnStoppedFunc(func(ctx sadefine.Context) {
			logger.Debug("OnStopped1")
		}, func(ctx sadefine.Context) {
			logger.Debug("OnStopped2")
		}),
		WithOnRestartingFunc(func(ctx sadefine.Context) {
			logger.Debug("OnRestarting1")
		}, func(ctx sadefine.Context) {
			logger.Debug("OnRestarting2")
		}))
	if err != nil {
		t.Errorf("Spawn fail, %v", err)
		return
	}

	startedWg.Wait()

	logger.Debugf("pid=%v", pid)

	timerMgr.NewTimer(1*time.Second, 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
		logger.Debug("timer")
	})

	timerMgr.NewTimer(srandom.Duration(1*time.Second, 3*time.Second), 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
		panic("panic test")
	})

	timerMgr.NewLoopTimer(1*time.Second, 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
		logger.Debug("loop timer")
	})

	time.Sleep(5 * time.Second)

	Stop(pid)

	stoppedWg.Wait()
}

func Test_OnStarted_Panic(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStartedFunc(func(ctx sadefine.Context) {
			log.Println("OnStarted")
			panic("test")
		}))
	if err != nil {
		log.Printf("Spawn fail, %v", err)
		return
	}

	Stop(pid)

	stoppedWg.Wait()
}

func Test_OnStopping_Panic(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStoppingFunc(func(ctx sadefine.Context) {
			log.Println("OnStopping")
			panic("test")
		}))
	if err != nil {
		log.Printf("Spawn fail, %v", err)
		return
	}

	Stop(pid)

	stoppedWg.Wait()
}

func Test_OnStopped_Panic(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStoppedFunc(func(ctx sadefine.Context) {
			log.Println("OnStopped")
			panic("test")
		}))
	if err != nil {
		log.Printf("Spawn fail, %v", err)
		return
	}

	Stop(pid)

	stoppedWg.Wait()
}

func Test_ParentChild(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	parentPID, err := Spawn(nil, true, "parent", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStartedFunc(func(ctx sadefine.Context) {
			log.Printf("parent OnStarted")

			_, err := Spawn(ctx, true, "child", true,
				WithOnStartedFunc(func(ctx sadefine.Context) {
					log.Printf("child OnStarted")
				}),
				WithOnStoppedFunc(func(ctx sadefine.Context) {
					log.Printf("child OnStopped")
				}),
			)
			if err != nil {
				log.Printf("Spawn child fail, %v", err)
				return
			}
		}),
		WithOnStoppedFunc(func(ctx sadefine.Context) {
			log.Printf("parent OnStopped")
		}),
	)
	if err != nil {
		log.Printf("Spawn parent fail, %v", err)
		return
	}

	time.Sleep(5 * time.Second)

	Stop(parentPID)

	stoppedWg.Wait()
}

func Test_Decider(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	parentPID, err := Spawn(nil, true, "parent", true,
		WithDecider(StopDecider),
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStartedFunc(func(ctx sadefine.Context) {
			log.Printf("parent OnStarted")

			var timerMgr *satimer.Manager

			_, err := Spawn(ctx, true, "child", true,
				WithOnStartedFunc(func(ctx sadefine.Context) {
					log.Printf("child OnStarted")
					timerMgr = GetTimerManager(ctx)
				}),
				WithOnStoppedFunc(func(ctx sadefine.Context) {
					log.Printf("child OnStopped")
				}),
			)
			if err != nil {
				log.Printf("Spawn child fail, %v", err)
				return
			}
			timerMgr.NewTimer(srandom.Duration(1*time.Second, 3*time.Second), 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
				panic("test")
			})
		}),
		WithOnStoppedFunc(func(ctx sadefine.Context) {
			log.Printf("parent OnStopped")
		}),
	)
	if err != nil {
		log.Printf("Spawn parent fail, %v", err)
		return
	}

	time.Sleep(3 * time.Second)

	Stop(parentPID)

	stoppedWg.Wait()
}

func Test_OnReceiveMessageFunc(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	type testMessage struct {
		content string
	}

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnReceiveMessageFunc(reflect.TypeOf((*testMessage)(nil)),
			func(ctx sadefine.Context) (continued bool) {
				msg := ctx.Message().(*testMessage)
				log.Printf("OnReceiveMessageFunc1 %s", msg.content)
				return true
			},
			func(ctx sadefine.Context) (continued bool) {
				msg := ctx.Message().(*testMessage)
				log.Printf("OnReceiveMessageFunc2 %s", msg.content)
				return false
			},
			func(ctx sadefine.Context) (continued bool) {
				log.Printf("error happend, this func must not be execute")
				return true
			}),
	)
	if err != nil {
		log.Printf("Spawn fail, %v", err)
		return
	}

	Send(pid, &testMessage{content: "test"})

	time.Sleep(1 * time.Second)

	Stop(pid)

	stoppedWg.Wait()
}

func Test_OnReceiveMessageFunc_Panic(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup

	type testMessage struct {
	}

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnReceiveMessageFunc(reflect.TypeOf((*testMessage)(nil)),
			func(ctx sadefine.Context) (continued bool) {
				log.Printf("OnReceiveMessageFunc1")
				return true
			},
			func(ctx sadefine.Context) (continued bool) {
				log.Printf("OnReceiveMessageFunc2")
				panic("test")
				return true
			},
			func(ctx sadefine.Context) (continued bool) {
				log.Printf("OnReceiveMessageFunc3")
				return true
			}),
	)
	if err != nil {
		log.Printf("Spawn fail, %v", err)
		return
	}

	Send(pid, &testMessage{})

	time.Sleep(1 * time.Second)

	Stop(pid)

	stoppedWg.Wait()
}

func Test_HttpRequest(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup
	var logger slog.ILogger
	var timerMgr *satimer.Manager
	var httpRequestMgr *sahttpreq.Manager

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStartedFunc(func(ctx sadefine.Context) {
			logger = GetLogger(ctx)
			timerMgr = GetTimerManager(ctx)
			httpRequestMgr = sahttpreq.NewManager(RootContext(), ctx.Self(), logger)
		}),
	)
	if err != nil {
		t.Errorf("Spawn fail, %v", err)
		return
	}

	timerMgr.NewLoopTimer(1*time.Second, 0, func(ctx sadefine.Context, id satimer.ID, tag satimer.Tag) {
		id, err := httpRequestMgr.Request(0, 0, func() (req *http.Request, err error) {
			req, err = http.NewRequest(http.MethodGet, "https://www.baidu.com", nil)
			return
		}, func(err error, req *sahttpreq.Request, ctx sadefine.Context) {
			if err != nil {
				t.Errorf("http request fail, %s", err)
				return
			}

			_, e := req.GetResponseBodyData()
			if e != nil {
				t.Errorf("GetResponseBodyData fail, %s", e)
				return
			}

			//logger.Debugf("recv %s", body)

			logger.Debugf("http request[%d] end", req.ID())
		})
		if err != nil {
			t.Errorf("http request fail, %v", err)
			return
		}
		logger.Debugf("http request[%d] begin", id)
	})

	time.Sleep(5 * time.Second)

	Stop(pid)

	stoppedWg.Wait()
}

func Test_HttpServer(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	var stoppedWg sync.WaitGroup
	var logger slog.ILogger
	var httpRequestMgr *sahttpreq.Manager
	var httpServer *sahttpsvr.Server

	pid, err := Spawn(nil, true, "Test", true,
		WithStoppedWaitGroup(&stoppedWg),
		WithOnStartedFunc(func(ctx sadefine.Context) {
			logger = GetLogger(ctx)

			httpRequestMgr = sahttpreq.NewManager(RootContext(), ctx.Self(), logger)

			httpServer = sahttpsvr.New(RootContext(), ctx.Self(), logger)
			if listenAddr, err := httpServer.ListenAndServe("localhost:8080"); err != nil {
				t.Errorf("listen localhost:8080 fail")
				ctx.Stop(ctx.Self())
				return
			} else {
				logger.Debugf("listen and serve in %s", listenAddr)
			}

			httpServer.MustRegisterHandler(http.MethodGet, "/echo",
				sahttpsvr.NewHandlerOption(sahttpsvr.WithRequestQueued(true)),
				sahttpsvr.TraceRequestBody(logger),
				func(context *sahttpsvr.Context) {
					if data, e := io.ReadAll(context.Request.Body); e != nil {
						t.Errorf("get request body data fail, %s", e)
					} else {
						if len(data) > 0 {
							_, e = context.Writer.Write(data)
							if e != nil {
								t.Errorf("write data fail, %s", e)
								return
							}
							logger.Debugf("echo [%s]", data)
						}
					}
				})
		}),
	)
	if err != nil {
		t.Errorf("Spawn fail, %v", err)
		return
	}

	for i := 0; i < 5; i++ {
		id, err := httpRequestMgr.Request(0, 0, func() (req *http.Request, err error) {
			req, err = http.NewRequest(http.MethodGet, "http://localhost:8080/echo", bytes.NewReader([]byte(strconv.Itoa(i))))
			return
		}, func(err error, req *sahttpreq.Request, ctx sadefine.Context) {
			if err != nil {
				t.Errorf("request fail, %s", err)
				return
			}

			body, e := req.GetResponseBodyData()
			if e != nil {
				t.Errorf("GetResponseBodyData fail, %s", e)
				return
			}

			logger.Debugf("request[%d] end, recv %s", req.ID(), body)
		})
		if err != nil {
			t.Errorf("http request fail, %v", err)
			return
		}
		logger.Debugf("request[%d] begin", id)
	}

	time.Sleep(3 * time.Second)

	Stop(pid)

	stoppedWg.Wait()
}
