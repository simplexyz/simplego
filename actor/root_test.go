package actor

import (
	"log"
	"testing"
)

func Test_Root(t *testing.T) {
	Init("test", ResumeDecider)
	defer Final()

	log.Printf("gRooPID=%v", gRootPID)
}
