package actor

import (
	sadefine "gitee.com/simplexyz/simplego/actor/define"
	aactor "github.com/asynkron/protoactor-go/actor"
)

func ResumeDecider(reason any) sadefine.Directive {
	return aactor.ResumeDirective
}

func RestartDecider(reason any) sadefine.Directive {
	return aactor.RestartDirective
}

func StopDecider(reason any) sadefine.Directive {
	return aactor.StopDirective
}
