package actor

import "errors"

var (
	ErrAlreadyStart = errors.New("already start")
	ErrNotStart     = errors.New("not start")
	ErrAlreadyStop  = errors.New("already stop")
)
