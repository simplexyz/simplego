package work

import "errors"

var (
	ErrDoNotPostOrDispatchNilWork = errors.New("do not post or dispatch nil work")
	ErrDispatchResultNotMatch     = errors.New("dispatch result not match")
)
