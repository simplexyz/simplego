package log

import slog "gitee.com/simplexyz/simplego/log"

var logger = slog.CreateConsoleLogger()

func Get() slog.ILogger {
	return logger
}

func Set(l slog.ILogger) {
	logger = l
}
