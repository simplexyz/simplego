package define

import (
	aactor "github.com/asynkron/protoactor-go/actor"
)

type (
	Actor = aactor.Actor

	ActorSystem = aactor.ActorSystem
	RootContext = aactor.RootContext

	Props       = aactor.Props
	PropsOption = aactor.PropsOption

	Directive   = aactor.Directive
	DeciderFunc = aactor.DeciderFunc

	Config       = aactor.Config
	ConfigOption = aactor.ConfigOption

	Context = aactor.Context
	PID     = aactor.PID

	Started    = aactor.Started
	Stopping   = aactor.Stopping
	Stopped    = aactor.Stopped
	Restarting = aactor.Restarting
	Terminated = aactor.Terminated

	TerminatedReason = aactor.TerminatedReason

	Future = aactor.Future
)
