package slog

import (
	"log/slog"
	"os"
	"testing"
)

func Test(t *testing.T) {
	jsonHandler := slog.NewJSONHandler(os.Stderr, &slog.HandlerOptions{
		AddSource:   false,
		Level:       slog.LevelDebug,
		ReplaceAttr: nil,
	})
	jsonLogger := slog.New(jsonHandler)
	jsonLogger.Debug("my first slog json msg", "hello", "slog")

	textHandler := slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		AddSource: false,
		Level:     slog.LevelDebug,
	})
	textLogger := slog.New(textHandler)
	textLogger.Debug("my first slog text msg", "hello", "slog")
}
